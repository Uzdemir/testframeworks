const request = require('request');

async function doRequest(host) {
	return new Promise((resolve) => {
		request.get({
			url : host,
			time : true
		}, async (err, res, body) => {
			if (err) return resolve({
				headers: [],
				time: 0,
				err: err
			});
			return resolve({
				headers: res.headers,
				time: res.elapsedTime,
				err: null
			});
		});
	});
}

async function doTest({
	connections,
	host
}) {
	return new Promise((resolve) => {
		let reqs = [];
		for (let i = 0; i < connections; i++) {
			reqs.push(doRequest(host));
		}
		Promise.all(reqs).then(results => {
			let time = 0;
			for (let i = 0; i < connections; i++){
				time+=results[i].time;
			} 
			if (time === 0)
				return resolve(0);
			time /= 1000;
			return resolve({
				qps: connections/time,
				spq: time/connections
			});
		});
	});
};

async function strategyTest ({
	connections,
	host,
}) {
	let r1 = await doTest({
		connections: connections, 
		host: host
	});
	return r1;
}

async function main() {
	let ServerJSLoadTest = await strategyTest({
		connections: 10000,
		host: 'http://localhost:3000'
	});
	let KoaJSLoadTest = await strategyTest({
		connections: 10000,
		host: 'http://localhost:3010'
	});
	let ExpressJSLoadTest = await strategyTest({
		connections: 10000,
		host: 'http://localhost:3020'
	});
	ServerJSLoadTest.headers = (await doRequest('http://localhost:3000')).headers;
	KoaJSLoadTest.headers = (await doRequest('http://localhost:3010')).headers;
	ExpressJSLoadTest.headers = (await doRequest('http://localhost:3020')).headers;
	
	console.log(JSON.stringify({
		KoaJS: KoaJSLoadTest,
		ServerJS: ServerJSLoadTest,
		ExpressJS: ExpressJSLoadTest
	}, null, '\t'));
}

main();