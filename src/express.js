const express = require('express');
const helmet = require('helmet');

const app = express();
let cpuUsed = {
	heapUsed:0
};
app.use(helmet());
app.get('/', (req, res) => {
	let currentUsed = process.memoryUsage();
	if ( currentUsed.heapUsed > cpuUsed.heapUsed)
		cpuUsed = currentUsed;
	console.log(JSON.stringify(cpuUsed, null, '\t'));
	return res.send('Hello');
});
app.listen(3020);