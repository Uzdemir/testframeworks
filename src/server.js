const server = require('server');
const {get, post} = server.router;
let cpuUsed = {
	heapUsed:0
};
server({
	port: 3000
},
	get('/', async ctx => {
		let currentUsed = process.memoryUsage();
		if ( currentUsed.heapUsed > cpuUsed.heapUsed)
			cpuUsed = currentUsed;
		console.log(JSON.stringify(cpuUsed, null, '\t'));	
		return 'Hello';
	})
);