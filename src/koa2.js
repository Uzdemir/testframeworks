const Koa = require('koa');
const Router = require('koa-router');
const helmet = require('koa-helmet');

const app = new Koa();
const router = new Router();
let cpuUsed = {
	heapUsed:0
};
router.get('/', async ctx => {
	let currentUsed = process.memoryUsage();
	if ( currentUsed.heapUsed > cpuUsed.heapUsed)
		cpuUsed = currentUsed;
	console.log(JSON.stringify(cpuUsed, null, '\t'));
	ctx.body = 'Hello';
});
app.use(helmet());
app.use(router.routes());
app.listen(3010);